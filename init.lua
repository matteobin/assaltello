arena_lib.register_minigame("assaltello", {
	name = "Assaltello",
	teams = {"Cavalieri", "Assalitori"}, -- 1, 2
	teams_color_overlay = {"blue", "red"},
	is_team_chat_default = true,
	hotbar = {
		slots = 2,
		background_image = "arenalib_gui_hotbar2.png"
	},
	time_mode = "decremental",
	disable_inventory = true,
	can_drop = false,

	properties = {
		prigione = {x=0,y=0,z=0}
	},

	temp_properties = {
		re = {},
	}
})



-- fasi dell'arena: caricamento -> gioco -> celebrazione
arena_lib.on_load("assaltello", function(arena)
	for pl_name, _ in pairs(arena.players) do	-- arena.players = nome / dati (teamID)
		local inv = minetest.get_player_by_name(pl_name):get_inventory()
		inv:add_item("main", "default:sword_steel")
		inv:add_item("main", "default:apple 5")
	end

	arena_lib.HUD_send_msg_team("hotbar", arena, 1, "Difendi il re!")
	arena_lib.HUD_send_msg_team("hotbar", arena, 2, "Annienta il re!")
end)

arena_lib.on_start("assaltello", function(arena)
	arena_lib.HUD_send_msg_all("hotbar", arena, "Tempo rimasto: " .. arena.initial_time)

	arena.re = minetest.add_entity(arena.spawn_points[1][1], "assaltello:re", arena.name)
end)

arena_lib.on_end("assaltello", function(arena)
	if arena.re then
		arena.re:remove()
	end
end)



-- comportamento del tempo
arena_lib.on_timeout("assaltello", function(arena)
	arena_lib.load_celebration("assaltello", arena, 1)
	arena_lib.HUD_hide("hotbar", arena)
end)

arena_lib.on_time_tick("assaltello", function(arena)
	arena_lib.HUD_send_msg_all("hotbar", arena, "Tempo rimasto: " .. arena.current_time)
end)


-- comportamento giocatore
arena_lib.on_death("assaltello", function(arena, p_name, reason)
	if arena.players[p_name].teamID == 1 then return end

	local player = minetest.get_player_by_name(p_name)
	local p_meta = player:get_meta()

	 p_meta:set_int("assaltello_prigione", 1)

	 minetest.after(5, function()
		if player then -- potrebbe essersi disconnessə
			p_meta:set_int("assaltello_prigione", 0)

			-- se è in prigione, teletrasportalə sul primo punto di rinascita assalitore
			if arena.players[p_name] and player:get_hp() > 0 then
				player:set_pos(arena.spawn_points[2][1])
			end
		end
	end)
end)

arena_lib.on_respawn("assaltello", function(arena, p_name)
	if arena.players[p_name].teamID == 1 then return end

	local player = minetest.get_player_by_name(p_name)
	local p_meta = player:get_meta()

	if p_meta:get_int("assaltello_prigione") == 1 then
		player:set_pos(arena.prigione)
	end
end)





local re = {
	initial_properties = {
		hp_max = 100,
		physical = true,
		collisionbox = {-0.3, 0.0, -0.3, 0.3, 1.7, 0.3},
		visual = "mesh",
		mesh = "character.b3d",
		textures = {"character.png"},
		nametag = "Re - 100♥"
	},

	-- dove salveremo l'arena associata al re, da usare nei richiami che seguono
	arena = {},

	-- se il server crasha, rimuoviamo istantaneamente il re rimasto al riavvio
	-- (staticdata equivale al nome dell'arena se invocato dalla mod, sennò è vuoto)
	on_activate = function(self, staticdata, dtime_s)
		if staticdata == "" then
			self.object:remove()
			return
		end

		local _, arena = arena_lib.get_arena_by_name("assaltello", staticdata)
		self.arena = arena
	end,

	on_death = function(self, killer)
		arena_lib.load_celebration("assaltello", self.arena, 2)
	end,

	on_punch = function(self, puncher, v1, v2, v3, damage)
		if self.arena.in_celebration then return true end

		local p_name = puncher:get_player_name()

		if self.arena.players[p_name].teamID == 1 then
			minetest.chat_send_player(p_name, "Lunga vita al re! (Ao, che stai a fa'?)")
			return true
		end

		local king = self.object

		king:set_nametag_attributes({text = "Re - " .. king:get_hp() - damage .. "♥"})
	end
}

minetest.register_entity("assaltello:re", re)
