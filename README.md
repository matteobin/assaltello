# Assaltello
Minigioco per Minetest realizzato con [arena_lib](https://content.minetest.net/packages/Zughy/arena_lib/)

### Note
* Assaltello è un minigioco realizzato durante un laboratorio di 2 ore e mezza, che può essere esteso e migliorato. Per provare minigiochi più rifiniti, vedasi il server [AES](https://aes.land/it)
* Come specificato sul README di `arena_lib`, le mod che alterano il punto di rinascita non faranno funzionare le prigioni. Usando Minetest Game, la mod integrata `beds` sarà quindi d'impiccio 
* Considerate di lasciare una recensione ad arena_lib [su ContentDB](https://content.minetest.net/packages/Zughy/arena_lib/#reviews): ciò permette di dare più visibilità alla mod
* Se non vi è chiaro qualche passaggio, [questa](https://gitlab.com/zughy-friends-minetest/arena_lib/-/wikis/Your-first-minigame) è la wiki inglese, sulle note della quale è stato realizzato il laboratorio
* Pubblicità progresso: fate un salto sullo [spazio Matrix](https://matrix.to/#/#videogiochiliberi:matrix.org) di Videogiochi Liberi, dove si parla di questo e altro

### Licenza
CC0